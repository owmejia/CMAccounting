package com.banquito.accounting;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = AccountingApplicationTests.class)
class AccountingApplicationTests {

	@Test
	void contextLoads() {
	}

}
