package com.banquito.accounting.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_MODIFIED)
public class ModifiedException extends RuntimeException {

  public ModifiedException(String message) {
    super(message);
  }
}
