package com.banquito.accounting.resource;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.banquito.accounting.dto.GeneralLedgerReportDTO;
import com.banquito.accounting.dto.JournalEntryDTO;
import com.banquito.accounting.dto.JournalEntryDetailDTO;
import com.banquito.accounting.dto.PeriodDTO;
import com.banquito.accounting.dto.TransactionDTO;
import com.banquito.accounting.model.JournalEntry;
import com.banquito.accounting.model.JournalEntryDetail;
import com.banquito.accounting.service.JournalEntryService;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping(path = "/JournalEntry")
@RequiredArgsConstructor
public class JournalEntryResource {
    private final JournalEntryService service;

    @GetMapping
    public ResponseEntity<List<JournalEntry>> getJournalEntries() {
      return ResponseEntity.ok(this.service.getJournalEntries());
    }

    @GetMapping(path = "/AccountingPeriod")
    public ResponseEntity<List<JournalEntry>> getJournalEntryInAccountingPeriod(@RequestBody() PeriodDTO dto){
      return ResponseEntity.ok(this.service.getByAccountingDate(dto.getDateFrom(), dto.getDateTo()));
    }

    @GetMapping(path = "/Report/{dateFrom}/{dateTo}")
    public ResponseEntity<List<GeneralLedgerReportDTO>> getGLReportInAccountingPeriod(@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") Date dateFrom, @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") Date dateTo){
      return ResponseEntity.ok(this.service.getReportByAccountingDate(dateFrom, dateTo));
    }

    @GetMapping(path = "/TransactionReference")
    public ResponseEntity<JournalEntry> getJournalEntryByTransactionReference(@RequestParam String transactionReference){
      return ResponseEntity.ok(this.service.searchByTransactionReference(transactionReference));
    }

    @GetMapping(path = "/GeneralLedgerAccount")
    public ResponseEntity<List<JournalEntry>> getJournalEntryByGeneralLedgerAccount(@RequestParam String generalLedgerAcount){
      return ResponseEntity.ok(this.service.getByGeneralLedgerAccount(generalLedgerAcount));
    }

    @PostMapping
    public ResponseEntity<JournalEntryDTO> create(@RequestBody TransactionDTO dto) {
      try {
        JournalEntry journalEntry = this.service.create(dto.getTransactionReference(), dto.getAmount(), dto.getType(), dto.getTransactionDate());
        return ResponseEntity.ok(this.buildJournalEntryDTO(journalEntry));
      } catch (Exception e) {
        e.printStackTrace();
        return ResponseEntity.badRequest().build();
      }
    }

    private JournalEntry buildJournalEntry(JournalEntryDTO dto){
        JournalEntry journalEntry =
            JournalEntry.builder()
                .transactionReference(dto.getTransactionReference())
                .accountingDate(dto.getAccountingDate())
                .transactionDate(dto.getTransactionDate())
                .memo(dto.getMemo())
                .build();
        List<JournalEntryDetail> journalEntryDetails = new ArrayList<>();
        for (JournalEntryDetailDTO journalEntryDetailDTO : dto.getJournalEntries()) {
            journalEntryDetails.add(
                JournalEntryDetail.builder()
                    .ledgerAccount(journalEntryDetailDTO.getLedgerAccount())
                    .debit(journalEntryDetailDTO.getDebit())
                    .credit(journalEntryDetailDTO.getCredit())
                    .build());
        }
        journalEntry.setJournalEntries(journalEntryDetails);
        return journalEntry;
    }

    private JournalEntryDTO buildJournalEntryDTO(JournalEntry journalEntry){
        JournalEntryDTO dto =
            JournalEntryDTO.builder()
                .groupInternalId(journalEntry.getGroupInternalId())
                .transactionReference(journalEntry.getTransactionReference())
                .accountingDate(journalEntry.getAccountingDate())
                .transactionDate(journalEntry.getTransactionDate())
                .memo(journalEntry.getMemo())
                .build();
        List<JournalEntryDetailDTO> journalEntryDetailsDTO = new ArrayList<>();
        for (JournalEntryDetail journalEntryDetail : journalEntry.getJournalEntries()) {
            journalEntryDetailsDTO.add(
                JournalEntryDetailDTO.builder()
                    .ledgerAccount(journalEntryDetail.getLedgerAccount())
                    .debit(journalEntryDetail.getDebit())
                    .credit(journalEntryDetail.getCredit())
                    .build());
        }
        dto.setJournalEntries(journalEntryDetailsDTO);
        return dto;
    }
}
