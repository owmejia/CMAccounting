package com.banquito.accounting.resource;

import com.banquito.accounting.dto.GeneralLedgerAccountDTO;
import com.banquito.accounting.model.GeneralLedgerAccount;
import com.banquito.accounting.service.GeneralLedgerAccountService;
import lombok.RequiredArgsConstructor;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/GeneralLedgerAccount")
@RequiredArgsConstructor
public class GeneralLedgerAccountResource {
    private final GeneralLedgerAccountService ledgerAccountService;

    @GetMapping(path = "ledgerAccount")
  public ResponseEntity<List<GeneralLedgerAccount>> getGeneralLedgerAccount(){
    return ResponseEntity.ok(this.ledgerAccountService.listGeneralLedgerAccounts());
  }
    @GetMapping(path = "ledgerAccount/code/{code}")
    public ResponseEntity<GeneralLedgerAccount> getLedgerAccountByCode(@PathVariable("code") String code){
      return ResponseEntity.ok(this.ledgerAccountService.searchByCode(code));
    }

    /*
    @GetMapping(path = "ledgerAccount")
    public ResponseEntity<GeneralLedgerAccount> get(){
      return ResponseEntity.ok(this.ledgerAccountService);
    }
 */
    @GetMapping(path = "ledgerAccount/type/{type}")
    public ResponseEntity<GeneralLedgerAccount> getLedgerAccountByType(@PathVariable String type){
      return ResponseEntity.ok(this.ledgerAccountService.searchByType(type));
    }

    @PostMapping
    public ResponseEntity<GeneralLedgerAccountDTO> create(@RequestBody GeneralLedgerAccountDTO dto){
      try {
        GeneralLedgerAccount ledgerAccount = this.ledgerAccountService.create(this.buildLedgerAccount(dto));
        return  ResponseEntity.ok(this.buildLedgerAccountDTO(ledgerAccount));
      } catch (Exception e){
        e.printStackTrace();
        return ResponseEntity.badRequest().build();
      }
    }

    @PutMapping
    public ResponseEntity<GeneralLedgerAccountDTO> update(@RequestBody GeneralLedgerAccountDTO dto){
      try{
        GeneralLedgerAccount ledgerAccount = this.ledgerAccountService.update(this.buildLedgerAccount(dto));
          return ResponseEntity.ok(this.buildLedgerAccountDTO(ledgerAccount));
        }catch (Exception e) {
          e.printStackTrace();
          return ResponseEntity.badRequest().build();
        }
    }

    private GeneralLedgerAccount buildLedgerAccount(GeneralLedgerAccountDTO dto){
      return GeneralLedgerAccount.builder()
                        .name(dto.getName())
                        .code(dto.getCode())
                        .description(dto.getDescription())
                        .type(dto.getType())
                        .currentBalance(dto.getCurrentBalance())
                        .openingBalance(dto.getOpeningBalance())
                        .build();
    }

    private GeneralLedgerAccountDTO buildLedgerAccountDTO(GeneralLedgerAccount ledgerAccount){
      return GeneralLedgerAccountDTO.builder()
                .name(ledgerAccount.getName())
                .code(ledgerAccount.getCode())
                .description(ledgerAccount.getDescription())
                .type(ledgerAccount.getType())
                .currentBalance(ledgerAccount.getCurrentBalance())
                .openingBalance(ledgerAccount.getOpeningBalance())
                .build();
    }
}
