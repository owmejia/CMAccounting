package com.banquito.accounting.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import java.util.Date;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "journal_entry")
@TypeAlias("journal_entry")
public class JournalEntry {

    @Id private String id;

    private String groupInternalId;

    @Indexed(name = "idxu_journalEntry_transactionReference", unique = true)
    private String transactionReference;

    @Indexed(name = "idx_journalEntry_accountingDate", unique = false)
    private Date accountingDate;

    private Date transactionDate;

    private String memo;

    private List<JournalEntryDetail> journalEntries;
}
