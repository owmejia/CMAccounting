package com.banquito.accounting.model;

import lombok.Builder;
import lombok.Data;
import java.math.BigDecimal;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@Document(collection = "general_ledger_account")
@TypeAlias("general_ledger_account")
public class GeneralLedgerAccount {

    @Id private String id;

    @Indexed(name = "idxu_generalLedgerAccount_code", unique = true)
    private String code;

    private String name;

    private String description; 

    @Indexed(name = "idxu_generalLedgerAccount_type", unique = true)
    private String type;

    private BigDecimal currentBalance;

    private BigDecimal openingBalance;
}
