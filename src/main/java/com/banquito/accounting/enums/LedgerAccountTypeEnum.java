package com.banquito.accounting.enums;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public enum LedgerAccountTypeEnum {
  ASSETS("ASSETS", "Activos"),
  EXPENSES("EXPENSES", "Gastos"),
  INCOME("INCOME", "Ingresos");

  private final String value;
  private final String text;
}
